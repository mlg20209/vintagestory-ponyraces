using System;
using HarmonyLib;
using Vintagestory.API.MathTools;

namespace ponyraces
{
    [HarmonyPatch(typeof(Vintagestory.GameContent.EntityBehaviorHealth))]
    [HarmonyPatch("OnFallToGround")]
    class PatchBehaviorHealthOnFallToGround
    {
        // modifying:
        // https://github.com/anegostudios/vsessentialsmod/blob/7234a11adea32a44127b356ad715bf29d21a53ed/Entity/Behavior/BehaviorHealth.cs#L227
        // add fall damage reduction
        static bool Prefix(Vintagestory.GameContent.EntityBehaviorHealth __instance, ref Vec3d positionBeforeFalling)
        {
            var entity = __instance.entity;

            // fall damage reduction
            float fallDamageMultiplier = 2f - entity.Stats.GetBlended("fallResistance");
            fallDamageMultiplier = GameMath.Max(0f, fallDamageMultiplier); // make sure not negative

            // modify yDistance by fall damage multiplier
            double yDistance = Math.Abs(positionBeforeFalling.Y - entity.Pos.Y);
            positionBeforeFalling.Y = entity.Pos.Y + yDistance * fallDamageMultiplier;

            return true; // run original
        }
    }
}