using HarmonyLib;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;


namespace ponyraces
{
    [HarmonyPatch(typeof(Entity))]
    [HarmonyPatch("get_RenderColor")]
    class PatchEntityRenderColor
    {
        // modifying:
        // https://github.com/anegostudios/vsapi/blob/ae41df550efe3b64508c615479ee30db108af58f/Common/Entity/Entity.cs#L424
        // to implement nirik color
        static void Postfix(Entity __instance, ref int __result)
        {
            var nirik = __instance.GetBehavior<EntityBehaviorNirik>();
            // System.Console.WriteLine($"PatchEntityRenderColor Postfix col={__result} | nirik.Enabled? = {nirik?.Enabled}, nirik.NirikMode = {nirik?.NirikMode}");
            if (nirik != null && nirik.NirikMode)
            {
                __result = ColorUtil.ToRgba(255, 5, 5, 5);
            }
        }
    }
}