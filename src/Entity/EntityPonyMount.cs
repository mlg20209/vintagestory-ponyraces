using System;
using System.IO;
using System.Linq;
using ponyraces;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace ponyraces
{
    // based on:
    // https://github.com/anegostudios/vssurvivalmod/blob/2bfd75697208df8f1d5386e117f7f791df596cc6/Systems/Boats/EntityPonyMount.cs
    // https://github.com/anegostudios/vssurvivalmod/blob/2bfd75697208df8f1d5386e117f7f791df596cc6/Systems/Boats/BoatSeat.cs
    
    public class EntityPonyMount : Entity, IMountable, IMountableSupplier
    {

        public override bool ApplyGravity
        {
            get { return false; }
        }

        public override bool IsInteractable
        {
            get { return false; }
        }

        public IMountable[] MountPoints => Array.Empty<IMountable>();

        public Vec3f BaseMountOffset = new Vec3f(0.0f, 0.5f, 0.0f);

        ICoreClientAPI capi;

        // host entity this is attached to
        public Entity Host = null;
        public EntityBehaviorPonyFlight HostPonyFlight = null;
        public long HostEntityIdForInit;

        // entity mounted on this
        public EntityAgent Passenger = null;
        public long PassengerEntityIdForInit;

        public bool controllable;

        protected Vec3f eyePos = new Vec3f(0, 1, 0);

        public EntityControls controls = new EntityControls();

        public EntityControls Controls
        {
            get {
                return controls; 
            }
        }

        public static string MOUNT_ANIMATION_FLYING = "mountponyflying";
        public static string MOUNT_ANIMATION_GROUND = "mountponyground";

        public string SuggestedAnimation => HostPonyFlight?.IsFlying == true ? MOUNT_ANIMATION_FLYING : MOUNT_ANIMATION_GROUND;

        public IMountableSupplier MountSupplier => this;
        public EnumMountAngleMode AngleMode => EnumMountAngleMode.Push;
        public new Vec3f LocalEyePos => eyePos;
        public Entity MountedBy => Passenger;
        public bool CanControl => controllable;

        public EntityPonyMount()
        {
            controls.OnAction = OnControls;
        }

        public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
        {            
            base.Initialize(properties, api, InChunkIndex3d);

            // from vanilla:
            // "The mounted entity will try to mount as well, but at that time,
            // the boat might not have been loaded, so we'll try mounting on both ends.
            // (wtf?)
            if (HostEntityIdForInit != 0 && Host == null)
            {
                if (api.World.GetEntityById(HostEntityIdForInit) is EntityAgent entity)
                {
                    Host = entity;
                    HostPonyFlight = Host.GetBehavior<EntityBehaviorPonyFlight>();
                }
            }
            if (PassengerEntityIdForInit != 0 && Passenger == null)
            {
                if (api.World.GetEntityById(PassengerEntityIdForInit) is EntityAgent entity)
                {
                    entity.TryMount(this);
                }
            }
        }

        public override void OnGameTick(float dt)
        {
            EntityPlayer hostPlayer = Host as EntityPlayer;
            bool hostIsOnline = hostPlayer?.Player != null;
            bool hostIsFlying = HostPonyFlight?.IsFlying == true;

            // conditions to remove this mount entity when no longer needed
            if (
                Host == null ||
                Host?.State != EnumEntityState.Active ||
                Host?.Alive == false ||
                hostIsOnline == false ||
                (hostIsFlying == false && Passenger == null)
            ) {
                Passenger?.TryUnmount();
                Die(EnumDespawnReason.Removed);
                return;
            }

            // Sync position to host entity
            if (Host != null)
            {
                SidedPos.SetFrom(Host.SidedPos);
            }

            // set passenger yaw
            if (Passenger != null)
            {
                Passenger.BodyYaw = Host.SidedPos.Yaw;
            }
            
            base.OnGameTick(dt);
        }

        public virtual bool IsMountedBy(Entity entity) => Passenger == entity;

        public virtual Vec3f GetMountOffset(Entity entity)
        {
            return MountOffset;
        }

        public override void OnInteract(EntityAgent byEntity, ItemSlot itemslot, Vec3d hitPosition, EnumInteractMode mode)
        {
            return;
        }

        public void OnControls(EnumEntityAction action, bool on, ref EnumHandling handled)
        {
            // unmount on client, send to server
            // because server does not recognize this action lmao
            if (Api.Side == EnumAppSide.Client)
            {
                if (action == EnumEntityAction.Sneak && on)
                {
                    Api.Logger.Notification($"TRY UNMOUNTING OnControls: {action}, {on}");
                    Passenger?.TryUnmount();
                    controls.StopAllMovement();
                    Api.ModLoader.GetModSystem<PonyRaceMod>()?.ClientUnmountRequest(EntityId);
                }
            }
        }

        public override bool CanCollect(Entity byEntity) => false;

        public override void ToBytes(BinaryWriter writer, bool forClient)
        {
            base.ToBytes(writer, forClient);
            writer.Write(Host?.EntityId ?? 0);
            writer.Write(Passenger?.EntityId ?? 0);
        }

        public override void FromBytes(BinaryReader reader, bool fromServer)
        {
            base.FromBytes(reader, fromServer);

            long hostId = reader.ReadInt64();
            HostEntityIdForInit = hostId;

            long entityId = reader.ReadInt64();
            PassengerEntityIdForInit = entityId;
        }

        public virtual bool IsEmpty()
        {
            return Passenger == null;
        }

        public override WorldInteraction[] GetInteractionHelp(IClientWorldAccessor world, EntitySelection es, IClientPlayer player)
        {
            return base.GetInteractionHelp(world, es, player);
        }

        Vec4f tmp = new Vec4f();
        Vec3f transformedMountOffset = new Vec3f();
        public Vec3f MountOffset
        {
            get
            {
                var pos = SidedPos;
                modelmat.Identity();
                
                // todo
                modelmat.Rotate(0, pos.Yaw, 0);
                
                var rotvec = modelmat.TransformVector(tmp.Set(BaseMountOffset.X, BaseMountOffset.Y, BaseMountOffset.Z, 0));
                return transformedMountOffset.Set(rotvec.X, rotvec.Y, rotvec.Z);
            }
        }

        EntityPos mountPos = new EntityPos();
        Matrixf modelmat = new Matrixf();
        public EntityPos MountPosition
        {
            get
            {
                var pos = Host.SidedPos;
                var moffset = MountOffset;

                mountPos.SetPos(
                    pos.X + moffset.X,
                    pos.Y + moffset.Y,
                    pos.Z + moffset.Z
                );

                mountPos.SetAngles(
                    pos.Roll,
                    pos.Yaw,
                    pos.Pitch
                );

                return mountPos;
            }
        }

        public void DidMount(EntityAgent entityAgent)
        {
            if (Passenger != null && Passenger != entityAgent)
            {
                Passenger.TryUnmount();
                return;
            }

            Passenger = entityAgent;
        }

        public override void OnEntityDespawn(EntityDespawnData despawn)
        {
            base.OnEntityDespawn(despawn);

            Api.Logger.Notification($"DESPAWNING {EntityId} {despawn.Reason}");

            // cleanup passenger
            RemovePassenger();
        }

        public void DidUnmount(EntityAgent entityAgent)
        {
            Api.Logger.Notification($"UNMOUNTING {entityAgent} from {Host}");

            if (Passenger == null) return;

            RemovePassenger();

            // you should kill yourself NOW
            Die(EnumDespawnReason.Removed);
        }

        public void RemovePassenger()
        {
            EntityAgent passenger = Passenger;
            if (passenger == null) return;
            Passenger = null;

            // note this re-calls DidUnmount, need Passenger == null
            // check to avoid this recursing infinitely
            passenger.TryUnmount();

            var pesr = passenger.Properties?.Client.Renderer as EntityShapeRenderer;
            if (pesr != null)
            {
                pesr.xangle = 0;
                pesr.yangle = 0;
                pesr.zangle = 0;
            }

            passenger.Pos.Roll = 0;

            passenger.AnimManager?.StopAnimation(MOUNT_ANIMATION_FLYING);
            passenger.AnimManager?.StopAnimation(MOUNT_ANIMATION_GROUND);
        }

        public void MountableToTreeAttributes(TreeAttribute tree)
        {
            tree.SetString("className", "ponymount");
            tree.SetLong("entityId", EntityId);
        }
        
        public static IMountable GetMountable(IWorldAccessor world, TreeAttribute tree)
        {
            return world.GetEntityById(tree.GetLong("entityId")) as EntityPonyMount;
        }

        public void Dispose()
        {
            
        }
    }
}
