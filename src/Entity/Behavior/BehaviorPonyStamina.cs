using System;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;

namespace ponyraces
{
    /// <summary>
    /// This is used as common ability stamina stat for pony race specific
    /// abilities. E.g. bucking tree, kirin fire, etc. will use this
    /// stamina as a way to cooldown abilities.
    /// </summary>
    public class EntityBehaviorPonyStamina : EntityBehavior
    {
        // global stamina regen multiplier. this gives control over
        // global base ability regen time, since `entity.Stats.GetBlended`
        // defaults to 1 so base regen cannot be as easily tuned by that
        // alone.
        public static float RegenMultiplier = 0.5f;

        ITreeAttribute attribute;

        private float maxStaminaCached = -1f;
        private float currentStaminaCached = -1f;
        private float regenRateCached = -1f;

        public float CurrentStamina
        {
            get {
                return currentStaminaCached = attribute.GetFloat("currentStamina");
            }
            set {
                if (value != currentStaminaCached)
                {
                    currentStaminaCached = value;
                    attribute.SetFloat("currentStamina", value);
                    entity.WatchedAttributes.MarkPathDirty("ponystamina");
                }
            }
        }

        public float MaxStamina
        {
            get { return maxStaminaCached; }
            set {
                maxStaminaCached = value;
                attribute.SetFloat("maxStamina", value);
                entity.WatchedAttributes.MarkPathDirty("ponystamina");
            }
        }

        public float RegenRate
        {
            get { return regenRateCached; }
            set {
                regenRateCached = value;
                attribute.SetFloat("regen", value);
                entity.WatchedAttributes.MarkPathDirty("ponystamina");
            }
        }
        
        public EntityBehaviorPonyStamina(Entity entity) : base(entity)
        {

        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            base.Initialize(properties, typeAttributes);

            attribute = entity.WatchedAttributes.GetTreeAttribute("ponystamina");
            if (attribute == null)
            {
                entity.WatchedAttributes.SetAttribute("ponystamina", attribute = new TreeAttribute());
                MaxStamina = 1;
                CurrentStamina = 1;
                RegenRate = 1;
            }
            else{
                MaxStamina = attribute.GetFloat("maxStamina");
                CurrentStamina = attribute.GetFloat("currentStamina");
                RegenRate = attribute.GetFloat("regen");
            }

            entity.WatchedAttributes.RegisterModifiedListener("ponystamina", OnAttributeChanged);
            entity.WatchedAttributes.RegisterModifiedListener("stats", OnStatsChanged);

            OnStatsChanged(); // run initial update from attribute
        }

        /// <summary>
        /// Spam refresh attribute like a retard otherwise the reference goes
        /// stale when i change races wtf
        /// </summary>
        public void OnAttributeChanged()
        {
            // Console.WriteLine("attribute changed");
            attribute = entity.WatchedAttributes.GetTreeAttribute("ponystamina");
        }
        
        /// <summary>
        /// Update from stats. Note this gets spammed like 100 times unnecessarily
        /// during character creation.
        /// </summary>
        public void OnStatsChanged()
        {
            MaxStamina = entity.Stats.GetBlended("abilityStamina");
            RegenRate = entity.Stats.GetBlended("abilityStaminaRegen");
            CurrentStamina = Math.Min(CurrentStamina, MaxStamina);
        }

        public override void OnEntityRevive()
        {
            CurrentStamina = MaxStamina;
        }

        /// <summary>
        /// Consume stamina and return new player stamina.
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public float ConsumeStamina(float amount)
        {
            float currentStamina = CurrentStamina;
            float newStamina = GameMath.Clamp(currentStamina - amount, 0, MaxStamina);
            CurrentStamina = newStamina;
            return newStamina;
        }

        public override void OnGameTick(float deltaTime)
        {
            if (entity.State == EnumEntityState.Inactive)
            {
                return;
            }

            base.OnGameTick(deltaTime);

            float regen = deltaTime * RegenRate * RegenMultiplier;
            float newStamina = Math.Min(MaxStamina, CurrentStamina + regen);
            // Console.WriteLine($"[{entity.Api.Side}] Current stamina: {CurrentStamina}, Max stamina: {MaxStamina}, Regen: {regen}");
            CurrentStamina = newStamina;
        }

        public override string PropertyName()
        {
            return "ponystamina";
        }
    }
}
