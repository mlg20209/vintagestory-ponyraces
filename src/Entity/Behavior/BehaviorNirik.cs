using System;
using kemono;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

namespace ponyraces
{
    /// <summary>
    /// Implements kirin nirik mode.
    /// </summary>
    public class EntityBehaviorNirik : EntityBehavior
    {
        // mod instance
        private readonly PonyRaceMod mod;

        // behavior enabled flag
        public bool Enabled = false;

        // nirik mode enabled
        public bool NirikMode = false;

        // list of traits that enable this behavior
        public static string[] AllowedTraits = {"nirik"};

        // nirik fire particles
        public AdvancedParticleProperties ParticlesFireCyan = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Size = NatFloat.createUniform(1.0f, 0.8f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(150, 70, 235, 255),
            VertexFlags = 150,
            LifeLength = NatFloat.createUniform(0.5f, 0.1f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.2f),
                NatFloat.createUniform(0f, 0.1f),
                NatFloat.createUniform(0f, 0.2f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.2f),
                NatFloat.createUniform(0f, 0.07f),
                NatFloat.createUniform(0f, 0.2f)
            }
        };
        public AdvancedParticleProperties ParticlesFirePink = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Size = NatFloat.createUniform(0.8f, 0.3f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(125, 255, 35, 70),
            VertexFlags = 175,
            LifeLength = NatFloat.createUniform(0.5f, 0.1f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.25f),
                NatFloat.createUniform(0f, 0.12f),
                NatFloat.createUniform(0f, 0.25f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.25f),
                NatFloat.createUniform(0f, 0.1f),
                NatFloat.createUniform(0f, 0.25f)
            }
        };

        // nirik mode set nearby entities on fire:
        
        // how often to do nearby entity fire check
        public int FireCheckTickPeriod = 4; // 8 * 1/33 ~ 0.12 seconds
        public int FireCheckCounter = 0;

        // entity fire check radius and height
        public float FireRadius = 1.2f;
        public float FireHeight = 1f;

        public EntityBehaviorNirik(Entity entity) : base(entity)
        {
            mod = entity.Api.ModLoader.GetModSystem<PonyRaceMod>();
        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            base.Initialize(properties, typeAttributes);

            // entity.WatchedAttributes.SetDouble("temporalStability", 0.6); // for debugging

            entity.WatchedAttributes.RegisterModifiedListener("characterRace", OnRaceChanged);

            OnRaceChanged(); // run initial update from stats
        }

        /// <summary>
        /// Update if race changes.
        /// </summary>
        public void OnRaceChanged()
        {
            var kemono = entity.World.Api.ModLoader.GetModSystem<KemonoMod>();

            Enabled = kemono.HasAnyRaceTrait(entity, AllowedTraits);
        }

        /// <summary>
        /// Update on each game tick.
        /// Nirik behavior:
        /// - When HP below threshold, reduce temporal stability
        /// - When temporal stability below threshold, turn on nirik fire mode
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void OnGameTick(float deltaTime)
        {
            if (entity.State == EnumEntityState.Inactive)
            {
                return;
            }

            base.OnGameTick(deltaTime);

            if (Enabled)
            {
                var healthTree = entity.WatchedAttributes.GetTreeAttribute("health");
                var healthPercent = healthTree?.GetFloat("currenthealth") / healthTree?.GetFloat("maxhealth") ?? 1f;
                
                // server: health below threshold, cause temporal stability loss
                // if temporal stability below threshold, enable nirik mode
                if (entity.World.Side == EnumAppSide.Server)
                {
                    double stability = entity.WatchedAttributes.GetDouble("temporalStability", 1);
                    double newStability = stability;

                    if (healthPercent < mod.Config.NirikHealthStabilityLossThreshold)
                    {
                        newStability = stability - (mod.Config.NirikHealthStabilityLossThreshold - healthPercent) * mod.Config.NirikHealthStabilityLossRate * deltaTime;
                        newStability = GameMath.Clamp(newStability, 0, 1);
                        entity.WatchedAttributes.SetDouble("temporalStability", newStability);
                    }

                    if (newStability < mod.Config.NirikTemporalStabilityThreshold)
                    {
                        if (NirikMode == false)
                        {
                            // enable eye glow
                            if (entity.World.Side == EnumAppSide.Server)
                            {
                                var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                                if (skin != null)
                                {
                                    skin.SetSkinPartGlow("eye", 255, false);
                                    entity.WatchedAttributes.MarkPathDirty("skinConfig");
                                }
                            }
                        }

                        NirikMode = true;

                        // set nearby entities on fire
                        FireCheckCounter += 1;
                        if (FireCheckCounter >= FireCheckTickPeriod)
                        {
                            FireCheckCounter = 0;
                            Entity[] entities = entity.World.GetEntitiesAround(
                                entity.SidedPos.XYZ,
                                FireRadius,
                                FireHeight,
                                (e) => e is EntityAgent && e.Alive && e.EntityId != entity.EntityId
                            );

                            foreach (var nearbyEntity in entities)
                            {
                                nearbyEntity.WatchedAttributes.SetBool("onFire", true);
                            }
                        }
                    }
                    else
                    {
                        if (NirikMode == true)
                        {
                            // disable eye glow
                            if (entity.World.Side == EnumAppSide.Server)
                            {
                                var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                                if (skin != null)
                                {
                                    skin.SetSkinPartGlow("eye", 0, false);
                                    entity.WatchedAttributes.MarkPathDirty("skinConfig");
                                }
                            }
                        }

                        NirikMode = false;
                    }
                }

                // client: if stability below threshold, enable nirik mode
                else if (entity.World.Side == EnumAppSide.Client)
                {
                    double stability = entity.WatchedAttributes.GetDouble("temporalStability", 1);
                    
                    if (stability < mod.Config.NirikTemporalStabilityThreshold)
                    {
                        NirikMode = true;

                        // create particles
                        float velMultiplier = 6; // tune this to look good
                        Vec3d pos = entity.Pos.XYZ.Add(0, 0.3, 0);
                        Vec3f vel = entity.Pos.Motion.ToVec3f().Mul(velMultiplier).AddCopy(0, 0.2f, 0);

                        // cyan
                        ParticlesFireCyan.basePos = pos;
                        ParticlesFireCyan.baseVelocity = vel.AddCopy(0, 0.4f, 0);
                        entity.World.SpawnParticles(ParticlesFireCyan);

                        // pink
                        pos = pos.AddCopy(0.0, 0.3, 0.0);
                        ParticlesFirePink.basePos = pos;
                        ParticlesFirePink.baseVelocity = vel.AddCopy(0, 0.6f, 0);
                        entity.World.SpawnParticles(ParticlesFirePink);
                    }
                    else
                    {
                        // disable nirik mode
                        NirikMode = false;
                    }
                }
            }
            else
            {
                // disable nirik mode
                if (NirikMode)
                {
                    NirikMode = false;

                    // disable eye glow
                    if (entity.World.Side == EnumAppSide.Server)
                    {
                        var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                        if (skin != null)
                        {
                            skin.SetSkinPartGlow("eye", 0, false);
                            entity.WatchedAttributes.MarkPathDirty("skinConfig");
                        }
                    }
                }
            }
        }

        public override string PropertyName()
        {
            return "nirik";
        }
    }
}
