using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;

namespace ponyraces
{
    /// <summary>
    /// Implements kirin nirik mode.
    /// </summary>
    public class EntityBehaviorPonyLevitate : EntityBehavior
    {
        // mod instance
        private readonly PonyRaceMod mod;

        // target entity being levitated
        public Entity Target {get; private set;} = null;

        // reference to same entity hunger behavior
        private Vintagestory.GameContent.EntityBehaviorHunger hungerBehavior;

        // reference to same entity stamina behavior
        private EntityBehaviorPonyStamina staminaBehavior;

        // initial distance to target entity when starting levitate
        private float distanceToTarget = 0f;
        
        // previous velocity set on target entity
        private Vec3d prevVelocity = new Vec3d(0, 0, 0);

        // velocity component exp averaging factor
        public float VelocityExpAvgFactor = 0.5f; 

        public AdvancedParticleProperties ParticlesSelf = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Size = NatFloat.createUniform(0.2f, 1.0f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(255, 255, 255, 70),
            VertexFlags = 200,
            LifeLength = NatFloat.createUniform(0.4f, 0.1f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 1.0f),
                NatFloat.createUniform(0f, 1.0f),
                NatFloat.createUniform(0f, 1.0f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0.5f, 0.5f),
                NatFloat.createUniform(0f, 0.5f)
            }
        };

        public AdvancedParticleProperties ParticlesLevitateTarget = new AdvancedParticleProperties
        {
            ParticleModel = EnumParticleModel.Quad,
            Size = NatFloat.createUniform(0.7f, 1.0f),
            HsvaColor = null,
            Color = ColorUtil.ToRgba(255, 255, 255, 70),
            VertexFlags = 200,
            LifeLength = NatFloat.createUniform(0.6f, 0.1f),
            GravityEffect = NatFloat.Zero,
            Velocity = new NatFloat[]
            {
                NatFloat.createUniform(0f, 1.2f),
                NatFloat.createUniform(0f, 1.2f),
                NatFloat.createUniform(0f, 1.2f)
            },
            PosOffset = new NatFloat[]
            {
                NatFloat.createUniform(0f, 0.5f),
                NatFloat.createUniform(0.5f, 0.5f),
                NatFloat.createUniform(0f, 0.5f)
            }
        };

        public EntityBehaviorPonyLevitate(Entity entity) : base(entity)
        {
            mod = entity.Api.ModLoader.GetModSystem<PonyRaceMod>();
        }

        public override void Initialize(EntityProperties properties, JsonObject typeAttributes)
        {
            base.Initialize(properties, typeAttributes);
        }

        public void StartLevitating(Entity target)
        {
            if (target == this.entity) return; // dont allow levitating self
            Target = target;

            // get reference to other affected behaviors
            staminaBehavior = entity.GetBehavior<EntityBehaviorPonyStamina>();
            hungerBehavior = entity.GetBehavior<Vintagestory.GameContent.EntityBehaviorHunger>();

            // try to turn on horn light to look cool with horn glowing
            entity.GetBehavior<EntityBehaviorPonyLight>()?.SetEnabled(true);

            // calculate initial distance to target
            distanceToTarget = (float) entity.ServerPos.DistanceTo(target.ServerPos);

            // refresh prev target velocity
            prevVelocity = new Vec3d(0, 0, 0);
        }

        public void StopLevitating()
        {
            Target = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void OnGameTick(float deltaTime)
        {
            if (entity.State == EnumEntityState.Inactive) return;
            if (Target == null) return;

            // consume hunger
            hungerBehavior?.ConsumeSaturation(mod.Config.LevitateHungerCost * deltaTime);

            // consume stamina
            var newStamina = staminaBehavior?.ConsumeStamina(mod.Config.LevitateStaminaCost * deltaTime);
            if (newStamina == null || newStamina <= 0)
            {
                StopLevitating();
                return;
            }

            if (entity.World.Side == EnumAppSide.Server)
            {
                Vec3f viewVector = entity.ServerPos.GetViewVector();
                Vec3d goalPos = entity.ServerPos.XYZ.AddCopy(
                    viewVector.X * distanceToTarget,
                    viewVector.Y * distanceToTarget,
                    viewVector.Z * distanceToTarget
                );

                // NAIVE WAY: move target directly to calculated position
                // PROBLEM: can move target through walls and blocks
                // Target.ServerPos.SetPos(goalPos);

                // ROBUST BUT MORE DIFFICULT:
                // calculate/apply velocity to move target to target position,
                // game physics engine will handle preventing movement
                // through blocks.
                // downside: looks wacky sometimes during large movements
                // (high velocity)

                // TODO: this method does not work for other players :^(
                // need special handling for player entities

                // calculate required velocity to reach target location
                Vec3d goalVelocity = goalPos.SubCopy(Target.ServerPos.XYZ).Mul(1.0/deltaTime); // m/s

                // clamp goal velocity magnitude within max throw velocity
                double mag = Math.Sqrt(
                    goalVelocity.X * goalVelocity.X +
                    goalVelocity.Y * goalVelocity.Y +
                    goalVelocity.Z * goalVelocity.Z
                );

                double magClamped = GameMath.Clamp(mag, 0, mod.Config.LevitateMaxThrowVelocity);

                Vec3d clampedVelocity = goalVelocity.Mul(magClamped / mag).Mul(deltaTime);

                // smooth velocity with exp averaging
                // prevents player spastic movements from causing weird
                // velocities when they move around target
                Vec3d avgVelocity = prevVelocity.Mul(VelocityExpAvgFactor).AddCopy(clampedVelocity.Mul(1 - VelocityExpAvgFactor));

                // apply smoothed velocity to target
                Target.ServerPos.Motion.Set(avgVelocity.X, avgVelocity.Y, avgVelocity.Z);

                // store current velocity for next frame
                prevVelocity = avgVelocity;
            }
            else if (entity.World.Side == EnumAppSide.Client)
            {
                // spawn particles around target and player

                // target
                ParticlesLevitateTarget.basePos = Target.Pos.XYZ;
                entity.World.SpawnParticles(ParticlesLevitateTarget);

                // player
                ParticlesSelf.basePos = entity.Pos.XYZ;
                entity.World.SpawnParticles(ParticlesSelf);

                // if not holding levitate key, stop levitating
                ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;
                if (entity == capi.World.Player.Entity)
                {
                    var key = capi.Input.HotKeys["ponylevitate"].CurrentMapping;
                    if (!capi.Input.KeyboardKeyState[key.KeyCode])
                    {
                        mod.ClientStopLevitate();
                        StopLevitating();
                    }
                }
            }
        }

        public override string PropertyName()
        {
            return "ponylevitate";
        }
    }
}
